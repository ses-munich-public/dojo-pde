# Parallel Letter Frequency

Count the frequency of letters in texts using parallel computation.
This exercise includes a sequential implementation as a
baseline in `src/lib.rs`.

# Parallel Letter Frequency in Rust

Learn more about concurrency in Rust here:

- [Concurrency](https://doc.rust-lang.org/book/ch16-00-concurrency.html)

# Benchmark

Benchmark your solution with the sequential implementation using a *nightly* Rust benchmark feature:

You need first to install the nightly rust implementation:

```
rustup toolchain install nightly
```

then run the benchmark with:

```
rustup run nightly cargo bench
```

- [Benchmark tests](https://doc.rust-lang.org/stable/unstable-book/library-features/test.html)

Learn more about nightly Rust:

- [Nightly Rust](https://doc.rust-lang.org/stable/book/2018-edition/appendix-06-nightly-rust.html)
- [Rustup: Working with nightly](https://github.com/rust-lang-nursery/rustup.rs#working-with-nightly-rust)


