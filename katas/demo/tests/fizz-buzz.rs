use demo;

#[test]
fn test_fizz_buzz() {
    let s = demo::fizz_buzz();
    let lines: Vec<&str> = s.lines().collect();
    print!("{}",s);
    assert_eq!("1", lines[0]);
    assert_eq!("Fizz", lines[2]);
    assert_eq!("Buzz", lines[4]);
    assert_eq!("7", lines[6]);
    assert_eq!("FizzBuzz", lines[44]);
    assert_eq!("FizzBuzz", lines[29]);
    assert_eq!("7", lines[6]);
    assert_eq!("98", lines[97]);
    assert_eq!("Fizz", lines[98]);
    assert_eq!("Buzz", lines[99]);
}
