use std::fmt::{Display, Formatter, Result};

pub struct Roman;

// A `trait` is like `interfaces` in Java or C# and
// are implemented using the `impl` keyword. 
// For more info about implementing traits see https://doc.rust-lang.org/rust-by-example/trait.html
//
// The `Display` `trait` is used to format the Roman number 
// for displaying purposes.  A basic example of how to implement it
//  can be found here https://doc.rust-lang.org/std/fmt/trait.Display.html
impl Display for Roman {
    fn fmt(&self, _f: &mut Formatter<'_>) -> Result {
        unimplemented!("Return a roman-numeral string representation of the Roman object");
    }
}

// The `From` trait is the canonical way to create a type from another other type.
// In this case, a Roman number from a unsigned-integer. 
// 
// For more info about check https://doc.rust-lang.org/rust-by-example/conversion/from_into.html
impl From<u32> for Roman {
    fn from(num: u32) -> Self {
        unimplemented!("Construct a Roman object from the '{}' number", num);
    }
}
