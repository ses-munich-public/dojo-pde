# Getting Started

## Prerequisites

- Git: https://git-scm.com/downloads
    For Windows users, install with:
    - `Git Bash`
    - `.sh` files associated with `Bash`
- Lightweight IDE like vscode, atom or sublime

## Installation

### 1. Install Rust

```
$ curl https://sh.rustup.rs -sSf | sh
```

Windows users can also download manually the file and in the windows explorer double-click it or right click and select *Execute with `Git Bash`*.

If it does not work, full instructions and alternatives are [here](https://doc.rust-lang.org/book/ch01-01-installation.html).

### 2. Clone

```
git clone https://gitlab.com/ses-munich-public/dojo-pde
```

# Katas Work out

Katas are located in `dojo-pde/katas` folder and they are organized in the suggested work out order.

## Starting a Kata

Go into the kata's folder e.g.:

```sh
$ cd katas/01-hello-world
```

There you'll find an folder structure like:

```
├── Cargo.toml
├── README.md
├── src
│   └── lib.rs
└── tests
    └── hello-world.rs
```

Read the `README.md` for instructions, place your kata solution into `src/lib.rs` and  validate your kata running the tests from `tests` folder with:

```bash
$ cargo test

test result: ok. 35 passed; 0 failed; 0 ignored; 0 measured; 1 filtered out
```

If all test are passed, you have finished, congratulations!!!

### Testing alternatives

If you wish to run all ignored tests without editing the tests source file, use:

```bash
$ cargo test -- --ignored
```

To run a specific test, for example `some_test`, you can use:

```bash
$ cargo test some_test
```

If the specific test is ignored use:

```bash
$ cargo test some_test -- --ignored
```

To format your solution, inside the solution directory use

```bash
cargo fmt
```

To see, if your solution contains some common ineffective use cases, inside the solution directory use

```bash
cargo clippy --all-targets
```

## Learning Rust

- [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/) shows you examples of the most common things you will be writing in Rust.

- [The Rust Programming Language](https://doc.rust-lang.org/book/) is a great resource for getting started with Rust as well as diving deeper into specific features of Rust.

## Credits

Coding exercises were based on the open-source [Exercism rust track](https://github.com/exercism/rust/tree/master/exercises) with some adjustments for the Dojo.