@snap[midpoint span-100]
# Coding Dōjō
@snapend

---?image=http://giphygifs.s3.amazonaws.com/media/AoSMPAREugAAo/giphy.gif&size=50% auto

## Dōjō

@snap[south span-80]

"A hall or space for immersive learning"

@snapend

---?image=https://media.giphy.com/media/fUSX3E4imAn9WHwrCV/giphy.gif&size=40% auto

## Kata

> "detailed choreographed pattern of movements made to be practiced as a way to memorize and perfect the movements being executed"

---?image=https://reach.rust-lang.org/static/rust-logo-white.png&size=30% auto

## Rust

Note:
- New: Most loved programming languague
- Interesting: Memory Safety, no garbage collector, safe concurrency
- Not used regulary

---

## Katas

@ul
- Hello, World!
- Leap Year
- Roman Numerals
- Simple Cipher
- Parallel Leter Frequency [Bonus]
@ulend

---

## Agenda

@ul
1. Quick Rust Intro
2. Dojo Instructions
3. Kata Live Demo
4. Dojo
5. Presenting Results
@ulend

---

# Rust Quick Intro

---

## Key Concepts

@ul
- Ownership & Scope
- Move
- Borrowing
@ulend

Note:
- the scope of the value is the same as the scope of the owner

### Variables

```rs

    let x: i32 = 1;

    let mut y = 1;

```

---

## Strings

  ```rs

  let s1 = "Hello, World!"
  let mut s2 = String::from("Hello");
  ```
@snap[south span-100 text-20]
@[2](literal string)
@[3](mutable string)
@snapend

---

## Arrays, Slices & Vectors


```rs
let arr = [4, 5, 6, 7];
let slice = &arr[..3];
let mut vec = vec![1, 2, 3];
vec.extend(slice);
assert_eq!(vec, [1, 2, 3, 4, 5, 6]);
```

@snap[south span-100]
@[1](fixed-size array of length 4: `[i32:4]`)
@[2](A view to an array `&[i32]`)
@[3-5](A growable vector)
@snapend

---

## if else


```rs

if {
    // expression
} else {
    // expression
}

```

---

## Loops

```rs

loop {
    if n > 100 { 
        break;
    }
}

```

---

## Loops

```rs

while n < 100 {

}

```

---

## Loops

```rs

for n in 1..101 {
    println!("{}", n);
}

```

---

## Patter Matching

```rs
let number = 13;
match number {
    1 => println!("One!"),
    2 | 3 | 5 | 7 | 11 => println!("This is a prime"),
    13..=19 => println!("A teen"),
    x if x > 50 => println!("Also a teen"),
    _ => println!("Ain't special"),
}

```

@snap[south span-100]
@[2](Match)
@[3](Match a single value)
@[4](Match several values)
@[5](Match an inclusive range)
@[6](Match with Guards)
@[7](Handle the rest of cases)

@snapend

---

### Key Resources


@snap[midpoint span-100]
@ul
- Rust by Examples: https://doc.rust-lang.org/rust-by-example/
- Rust Book: https://doc.rust-lang.org/book/ `rustup docs --book`

@ulend
@snapend

---?image=https://media.giphy.com/media/3o7btNhMBytxAM6YBa/giphy.gif&size=70% auto

---?image=https://media.giphy.com/media/xUNd9Ljg37yeVcCHyE/giphy.gif&size=50% auto&position=bottom

@snap[north]
## Let's Wram Up!
@snapend

---

## Prerequisites

@ul
- [x] Laptop (MacOs, Linux or Windows)
- [x] Git
    - [x] `Git Bash`
    - [x] Associate `.sh` with `Git Bash`
- Lightweight IDE like vscode, atom or sublime
@ulend

---

## Install Rust 

@snap[midpoint text-20]
https://rustup.rs/
@snapend

---

# Prepare IDE


```rs
let us = have("syntax highlighting")
```


---

@snap[midpoint]
# Kata Demo
@snapend

Note:

```rs
    let mut vec = vec![];
    for n in 1..=100 {
        if n % 15 == 0 {
            vec.push("FizzBuzz".to_owned());
            continue;
        }
        
        if n % 5 == 0 {
            vec.push("Buzz".to_owned());
            continue;
        } 
        
        if n % 3 == 0 {
            vec.push("Fizz".to_owned());
            continue;
        } 
        vec.push(n.to_string());
    }
    vec.join("\n")
```



---?image=https://media.giphy.com/media/LO4KIaBWEUTyE/giphy.gif&size=20 auto

@snap[midpoint h1-blue]
# Let's find a partner
@snapend

Note:
- Someone you do not know
- Both should code

---

## Clone Dojo Repo

@snap[midpoint]
```bash
git clone git@gitlab.com:ses-munich-public/dojo-pde.git
```
@snapend

---?image=https://media.giphy.com/media/MRH15ebBQboUMedyks/giphy.gif&color=white

@snap[midpoint h1-blue]
# Let's do it!
@snapend

---

# Thanks!